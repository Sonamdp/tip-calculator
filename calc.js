document.getElementById('container').onchange = function(){
  var total = Number(document.getElementById('totalInput').value);
  var tipPercent = document.getElementById('tipInput').value;
  var friends = document.getElementById('headCountInput').value;
  var totalTips = total * (tipPercent / 100);
  var newBillPerPerson = (total + totalTips) / friends;
  var newTipEach = totalTips / friends;
  
  document.getElementById('tipOutput').innerHTML = tipPercent + "%";
  document.getElementById('headCountOutput').innerHTML = friends;
  document.getElementById('newBill').innerHTML = "$ " + newBillPerPerson.toFixed(2);
  document.getElementById('tipEach').innerHTML = "$ " + newTipEach.toFixed(2);
};